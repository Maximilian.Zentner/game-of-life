package org.example.model;

public class GameFieldFactory {

  public static AbstractGameField createSmallGameField() {
    return new BorderedGameField(5, 5);
  }

  public static AbstractGameField createMediumGameField() {
    return new BorderedGameField(10, 10);
  }

  public static AbstractGameField createLargeGameField() {
    return new BorderedGameField(15, 15);
  }

  public static AbstractGameField createGameFieldForSize(int size) {
    if (size == 5) {
      return GameFieldFactory.createSmallGameField();
    } else if (size == 10) {
      return GameFieldFactory.createMediumGameField();
    } else if (size == 15) {
      return GameFieldFactory.createLargeGameField();
    } else {
      return new BorderedGameField(size, size);
    }
  }
}
