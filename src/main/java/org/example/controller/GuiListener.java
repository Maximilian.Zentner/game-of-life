package org.example.controller;

import java.awt.event.*;
import java.util.Scanner;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.MouseInputListener;
import org.example.controller.commands.Command;
import org.example.controller.commands.Commander;
import org.example.model.AbstractGameField;
import org.example.model.GameFieldObserver;
import org.example.view.Gui;

public class GuiListener
    implements MouseInputListener,
        ActionListener,
        ChangeListener,
        ComponentListener,
        GameFieldObserver {

  private final AbstractGameField field;
  private final Gui gui;

  private boolean running;
  private boolean restart = false;
  private Command currentCommand;

  private int sleepTime = 800;

  public GuiListener(AbstractGameField field, Gui gui, Boolean running) {
    this.field = field;
    this.gui = gui;
    this.running = running;
    this.field.addObserver(this);
  }

  @Override
  public void updateField(AbstractGameField field) {
    gui.getDraw().updateField(field.getField());
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    if (e.getButton() == MouseEvent.BUTTON1) {
      int x = e.getX();
      int y = e.getY();
      System.out.println(String.format("mouseClicked x=%s y=%s ", x, y));
    }
  }

  @Override
  public void mousePressed(MouseEvent e) {
    int x = e.getX();
    int y = e.getY();
    System.out.println(String.format("mousePressed x=%s y=%s ", x, y));

    if (e.getButton() == MouseEvent.BUTTON1) {
      activateCellAt(x, y, true);
    } else if (e.getButton() == MouseEvent.BUTTON3) {
      activateCellAt(x, y, false);
    }
  }

  private void activateCellAt(int x, int y, boolean enable) {

    int xTile = x / 40;
    int yTile = y / 40;
    int value = enable ? 1 : 0;

    this.field.setCell(yTile, xTile, value);
    //            field[yTile][xTile] = 1;
    gui.getDraw().updateField(field.getField());

    System.out.println("X Feld: " + xTile);
    System.out.println("Y Feld: " + yTile);
  }

  @Override
  public void mouseReleased(MouseEvent e) {}

  @Override
  public void mouseEntered(MouseEvent e) {}

  @Override
  public void mouseExited(MouseEvent e) {}

  @Override
  public void mouseDragged(MouseEvent e) {
    int x = e.getX();
    int y = e.getY();
    System.out.println(String.format("mouseDragged x=%s y=%s button=%s ", x, y, e.getButton()));

    if (SwingUtilities.isLeftMouseButton(e)) {
      activateCellAt(x, y, true);
    } else if (SwingUtilities.isRightMouseButton(e)) {
      activateCellAt(x, y, false);
    }
  }

  @Override
  public void mouseMoved(MouseEvent e) {}

  @Override
  public void actionPerformed(ActionEvent e) {
    System.out.println("Action performed:" + e.getActionCommand());
    switch (e.getActionCommand()) {
      case "Start":
        currentCommand = new Commander.StartCommand(this);
        break;
      case "Stop":
        currentCommand = new Commander.StopCommand(this);
        break;
      case "Restart":
        currentCommand = new Commander.RestartCommand(this, field, gui);
        break;
    }

    if (currentCommand != null) {
      currentCommand.execute();
    }
  }

  public boolean getRunning() {
    return running;
  }

  public int getSleepTime() {
    return sleepTime;
  }

  @Override
  public void stateChanged(ChangeEvent ce) {
    JSlider slider = (JSlider) ce.getSource();
    int myslider = slider.getValue();
    sleepTime = myslider * 1;
  }

  @Override
  public void componentResized(ComponentEvent e) {
    System.out.println(e.getComponent().getWidth() + " " + e.getComponent().getHeight());
    Gui.width = e.getComponent().getWidth();
    Gui.height = e.getComponent().getHeight();
  }

  @Override
  public void componentMoved(ComponentEvent e) {}

  @Override
  public void componentShown(ComponentEvent e) {}

  @Override
  public void componentHidden(ComponentEvent e) {}

  public static int getUserInputForFieldSize() {
    Scanner scanner = new Scanner(System.in);
    int size = 0;
    while (true) {
      System.out.print("Enter the size of the game field (5, 10, 15 or some more): ");
      if (scanner.hasNextInt()) {
        size = scanner.nextInt();
        if (size == 5 || size == 10 || size == 15 || size > 15) {
          break;
          //        } else {
          //          System.out.println("Invalid size. Please enter 5, 10, or 15.");
        }
        //      } else {
        //        System.out.println("Computer sagt NEIN");
        //        scanner.next();
      }
    }
    return size;
  }

  public void setRunning(boolean running) {
    this.running = running;
  }

  public void setRestart(boolean restart) {
    this.restart = restart;
  }
}
