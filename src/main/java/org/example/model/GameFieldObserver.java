package org.example.model;

import java.util.Observer;

public interface GameFieldObserver {
    void updateField(AbstractGameField field);
}
