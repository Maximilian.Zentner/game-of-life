package org.example.model;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractGameField {

  private int x;
  private int y;
  private int[][] field;

  protected AbstractGameField(int x, int y) {
    this.x = x;
    this.y = y;
    field = new int[x][y];
  }
  public int[][] getField() {
    return field;
  }

  public void setCell(int x, int y, int value) {
    if (isInsideField(x, y)) {
      field[x][y] = value;
    }
  }
  private List<GameFieldObserver> observers = new ArrayList<>();

  public void addObserver(GameFieldObserver observer) {
    observers.add(observer);
  }

  public void removeObserver(GameFieldObserver observer) {
    observers.remove(observer);
  }

  public void notifyObservers() {

    for (GameFieldObserver observer : observers) {
      observer.updateField(this);
      System.out.println("Ja des is as Feidl");
    }
  }

  private boolean isInsideField(int x, int y) {
    return 0 <= x && x < field.length && 0 <= y && y < field[0].length;
  }

  public void setField(int[][] field) {
    this.field = field;
  }

  public void fillField() {
    for (int i = 0; i < field.length; i++) {
      for (int j = 0; j < field.length; j++) {
        int temp = (int) Math.round(Math.random());
        field[i][j] = temp;

      }
    }
    notifyObservers();
  }

  public void printField() {
    for (int zeile = 0; zeile < field.length; zeile++) {
      for (int spalte = 0; spalte < field[0].length; spalte++) {
        if (field[zeile][spalte] == 0) System.out.print("[ ]");
        else System.out.print("[X]");
      }
      System.out.println();
    }
//    notifyObservers();
  }

  public int[][] nextGeneration(int[][] field, int X, int Y, int generation) {
    int[][] future = new int[X][Y];
    System.out.printf(
        "%s%s. Generation%s", System.lineSeparator(), generation, System.lineSeparator());
    for (int l = 0; l < Y; l++) {
      for (int m = 0; m < X; m++) {

        int aliveNeighbours = getNeighbours(field, X, Y, m, l);

        if ((field[l][m] == 1) && (aliveNeighbours < 2)) {
          future[l][m] = 0;
        } else if ((field[l][m] == 1) && (aliveNeighbours > 3)) {
          future[l][m] = 0;
        } else if ((field[l][m] == 0) && (aliveNeighbours == 3)) {
          future[l][m] = 1;
        } else {
          future[l][m] = field[l][m];
        }

      }
    }
//    notifyObservers();
    return future;
  }
  protected abstract int getNeighbours(int[][] field, int X, int Y, int m, int l);
}