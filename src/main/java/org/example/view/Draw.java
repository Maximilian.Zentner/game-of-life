package org.example.view;

import java.awt.*;
import javax.swing.*;

public class Draw extends JLabel {

  private int[][] field;
  private int height = 40;
  private int width = 40;

  public Draw() {}

  public void updateField(int[][] field) {
    this.field = field;
    repaint();
  }

  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2d = (Graphics2D) g;
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

    g.setColor(Color.LIGHT_GRAY);
    g.fillRect(0, 0, Gui.width, Gui.height);

    //    g.setColor(Color.GRAY);

    int fieldWidth = field[0].length;
    int fieldHeight = field.length;

    g.setColor(Color.BLACK);
    g.fillRect(0, 0, fieldWidth * width, fieldHeight * height);

    for (int x = 0; x < fieldWidth; x++) {
      for (int y = 0; y < fieldHeight; y++) {

        if (field[y][x] == 0) {
          //                    g.setColor(Color.BLACK);
          //                    g.fillRect(x * width, y * height, width, height);
        } else {
          g.setColor(Color.WHITE);
          g.fillRect(x * width, y * height, width, height);
        }
        g.setColor(Color.GRAY);
        g.drawRect(x * width, y * height, width, height);
      }
    }
  }
}
