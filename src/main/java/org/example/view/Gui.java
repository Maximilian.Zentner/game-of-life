package org.example.view;

import java.awt.*;
import javax.swing.*;
import org.example.controller.GuiListener;

public class Gui {

  JFrame jf;
  Draw d;
  public static int width = 600, height = 500;

  public void create(GuiListener guilistener) throws Exception {
    updateLAF();

    jf = new JFrame("Game of Life");
    jf.setSize(width, height);
    jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    jf.setLocationRelativeTo(null);
    jf.setLayout(new BorderLayout());

    jf.setResizable(true);
    // jf.addMouseListener(guilistener);

    initComponents(jf, guilistener);
  }

  private void initComponents(JFrame jf, GuiListener guilistener) {

    JPanel panel = new JPanel();
    JSlider speedSlider = getSpeedSlider();
    speedSlider.addChangeListener(guilistener);

    JButton startButton = new JButton("Start");
    startButton.addActionListener(guilistener);

    JButton stopButton = new JButton("Stop");
    stopButton.addActionListener(guilistener);

    JButton restartButton = new JButton("Restart");
    restartButton.addActionListener(guilistener);

    JButton saveButton = new JButton("Save");
    saveButton.addActionListener(guilistener);

    JButton loadButton = new JButton("Load");
    loadButton.addActionListener(guilistener);

    panel.add(startButton);
    panel.add(stopButton);
    panel.add(restartButton);
    panel.add(saveButton);
    panel.add(loadButton);

    panel.add(speedSlider);

    jf.add(panel, BorderLayout.PAGE_START);

    d = new Draw();
    d.setBounds(0, 0, width, height);
    d.setVisible(true);
    jf.add(d, BorderLayout.CENTER);
    d.addComponentListener(guilistener);
    d.addMouseListener(guilistener);
    d.addMouseMotionListener(guilistener);

    jf.requestFocus();
    jf.setVisible(true);

    jf.requestFocus();
  }

  private void updateLAF() throws Exception {

    UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
  }

  private static JSlider getSpeedSlider() {
    JSlider speedSlider = new JSlider(0, 2000, 800);

    // Die Abstände zwischen den
    // Teilmarkierungen werden festgelegt
    speedSlider.setMajorTickSpacing(400);
    speedSlider.setMinorTickSpacing(200);

    // Standardmarkierungen werden erzeugt
    speedSlider.createStandardLabels(200);

    // Zeichnen der Markierungen wird aktiviert
    speedSlider.setPaintTicks(true);

    // Zeichnen der Labels wird aktiviert
    speedSlider.setPaintLabels(true);

    speedSlider.setSnapToTicks(true);

    return speedSlider;
  }

  public Draw getDraw() {
    return d;
  }


}
