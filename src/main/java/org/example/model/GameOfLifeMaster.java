package org.example.model;

import static org.example.controller.GuiListener.getUserInputForFieldSize;
import static org.example.model.GameFieldFactory.createGameFieldForSize;

import org.example.controller.GuiListener;
import org.example.view.Gui;

public class GameOfLifeMaster {

  public static void main(String[] args) throws Exception {

    int size = getUserInputForFieldSize();
    //    int size = 25;
    AbstractGameField gameField = createGameFieldForSize(size);

    Gui gui = new Gui();
    Boolean running = true;
    GuiListener guilistener = new GuiListener(gameField, gui, running);
    gui.create(guilistener);

    gui.getDraw().updateField(gameField.getField());

    GameClock gc = GameClock.getInstance();
    gc.gameLoop(guilistener, gameField, gui, size, size);
  }
}
