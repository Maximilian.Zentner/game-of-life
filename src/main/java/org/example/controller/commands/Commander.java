package org.example.controller.commands;

import org.example.controller.GuiListener;
import org.example.model.AbstractGameField;
import org.example.view.Gui;

public class Commander {
  public static class StartCommand implements Command {
    private GuiListener listener;

    public StartCommand(GuiListener listener) {
      this.listener = listener;
    }

    @Override
    public void execute() {
      listener.setRunning(true);
    }
  }

  public static class StopCommand implements Command {
    private GuiListener listener;

    public StopCommand(GuiListener listener) {
      this.listener = listener;
    }

    @Override
    public void execute() {
      listener.setRunning(false);
    }
  }

  public static class RestartCommand implements Command {
    private GuiListener listener;
    private AbstractGameField field;
    private Gui gui;

    public RestartCommand(GuiListener listener, AbstractGameField field, Gui gui) {
      this.listener = listener;
      this.field = field;
      this.gui = gui;
    }

    @Override
    public void execute() {
      listener.setRestart(true);
      field.fillField();
      gui.getDraw().updateField(field.getField());
    }
  }
}
