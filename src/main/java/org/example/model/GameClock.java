package org.example.model;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.example.controller.GuiListener;
import org.example.view.Gui;

public class GameClock {

  private ExecutorService executorService = Executors.newFixedThreadPool(2);

  public GameClock() {

  }
  private static GameClock
      instance;

  public static GameClock getInstance() {
    if (instance == null) {
      instance = new GameClock();
    }
    return instance;
  }

  public void gameLoop(GuiListener guilistener, AbstractGameField field, Gui gui, int Y, int X) {

    int i = 0;
    field.fillField();

    while (true) {
      try {
        Thread.sleep(guilistener.getSleepTime());
        if (guilistener.getRunning()) {
          field.setField(field.nextGeneration(field.getField(), X, Y, i++));
          printField(field, gui);
        }
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      } catch (ExecutionException e) {
        throw new RuntimeException(e);
      }
    }
  }

  public void printField(AbstractGameField field, Gui gui)
      throws ExecutionException, InterruptedException {
    gui.getDraw().updateField(field.getField());
    var future = executorService.submit(field::printField);
    future.get();
  }
}