package org.example.model;

public class BorderedGameField extends AbstractGameField {

  public BorderedGameField(int x, int y) {
    super(x, y);
  }

  @Override
  protected int getNeighbours(int[][] field, int X, int Y, int m, int l) {
    int neighbors = 0;

    if (m > 0) {
      neighbors += field[l][m - 1];
    }
    if (m < X - 1) {
        neighbors += field[l][m + 1];
    }
    if (l > 0) {
        neighbors += field[l - 1][m];
    }
    if (l < Y - 1) {
        neighbors += field[l + 1][m];
    }

    if (m > 0 && l > 0) {
        neighbors += field[l - 1][m - 1];
    }
    if (m > 0 && l < Y - 1) {
        neighbors += field[l + 1][m - 1];
    }
    if (m < X - 1 && l > 0) {
        neighbors += field[l - 1][m + 1];
    }
    if (m < X - 1 && l < Y - 1) {
        neighbors += field[l + 1][m + 1];
    }

    return neighbors;
  }
}
